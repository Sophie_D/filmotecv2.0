import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import {FavorisService} from '../../Services/favorisService';


@Component({
  selector: 'app-listes-favourite-films',
  templateUrl: './listes-favourite-films.component.html',
  styleUrls: ['./listes-favourite-films.component.scss']
})
export class ListesFavouriteFilmsComponent implements OnInit {

  film:[];
  constructor(private favorisService:FavorisService) { }

  ngOnInit(): void {
    this.favorisService.addFavorite()
    .then(reponse => this.film = reponse.results);

  }
 //public addFilmtoFavorites():void{
   //let film :Film ={
   // original_title:this.film.original_title,
   // release_date:this.film.release_date,
   }
  // this.favorisService.addFavorite(film);

