import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Film } from '../Classes/Film';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
   })
   
export class FavorisService{
    constructor(private http: HttpClient) { }

    private savedMovies:Array<Film>;
 public addFavorite(movie: Film ):void {
       this.savedMovies.push(movie);
       localStorage.setItem('ALL_MOVIES', JSON.stringify(this.savedMovies))
      }

      public sendGetRequest(id: string){
        return this.http.get(' https://api.themoviedb.org/3/discover/movie?api_key=%27clé_api%27&language=fr&query=' + id);
      }
}